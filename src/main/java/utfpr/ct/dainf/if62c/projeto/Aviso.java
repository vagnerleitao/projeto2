package utfpr.ct.dainf.if62c.projeto;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimerTask;

/**
 * Linguagem Java
 * @author 
 */
public class Aviso extends TimerTask{
// Extensão da Classe Aviso para TimerTask    
    protected final Compromisso compromisso;

    public Aviso(Compromisso compromisso) {
       this.compromisso = compromisso;
    }

    @Override
    public void run() {
        Date data = new Date();
        data.setTime(compromisso.getData().getTime() - System.currentTimeMillis());
        SimpleDateFormat spd = new SimpleDateFormat("s"); //segundos
        System.out.println(compromisso.getDescricao() + " começa em " + spd.format(data) + "s");
    }
    
}
