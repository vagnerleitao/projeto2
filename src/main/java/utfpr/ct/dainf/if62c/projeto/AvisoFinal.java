package utfpr.ct.dainf.if62c.projeto;

/**
 * Linguagem Java
 * @author 
 */
public class AvisoFinal extends Aviso {

    public AvisoFinal(Compromisso compromisso) {
        super(compromisso);
    }

 @Override
    public void run() {
        System.out.println( compromisso.getDescricao() + " começa agora");
        compromisso.getAvisos().stream().forEach((i) -> {
            i.cancel();
        });
        
    }    

}
